{ config, pkgs, ... }:

{
  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Gnome Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Exclude unnecessary Gnome packages
  environment.gnome.excludePackages = with pkgs; [
    gnome-tour
    epiphany
    gnome-music
    totem
    tali
    iagno
    hitori
    atomix
    geary
  ];

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "fr";
    variant = "";
  };

  # Gnome extensions to install
  environment.systemPackages = with pkgs; [
    gnomeExtensions.appindicator
    gnomeExtensions.alphabetical-app-grid
    gnomeExtensions.clipboard-indicator
    gnomeExtensions.blur-my-shell
    gnomeExtensions.week-start-modifier
    gnomeExtensions.removable-drive-menu
    gnome-tweaks
    gnome-themes-extra
  ];
}
