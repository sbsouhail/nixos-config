{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./gnome.nix # Import the Gnome configuration here
    ./nvidia.nix # Import nvidia for laptops
  ];

  # Enable experimental features including flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Bootloader configuration
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Networking
  networking.hostName = "astro";
  networking.networkmanager.enable = true;

  # Time zone and locale settings
  time.timeZone = "Africa/Tunis";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "ar_TN.UTF-8";
    LC_IDENTIFICATION = "ar_TN.UTF-8";
    LC_MEASUREMENT = "ar_TN.UTF-8";
    LC_MONETARY = "ar_TN.UTF-8";
    LC_NAME = "ar_TN.UTF-8";
    LC_NUMERIC = "ar_TN.UTF-8";
    LC_PAPER = "ar_TN.UTF-8";
    LC_TELEPHONE = "ar_TN.UTF-8";
    LC_TIME = "ar_TN.UTF-8";
  };

  # User configuration
  users.users.sbsouhail = {
    isNormalUser = true;
    description = "Souhail SBOUI";
    extraGroups = [ "networkmanager" "wheel" "docker" ];
  };

  # Package configuration
  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    google-chrome
    vscode
    nixpkgs-fmt
    vlc
    git
    distrobox
    spotify
    discord
    dbeaver-bin
    mongodb-compass
    thunderbird
    postman
    unzip
    openssl
    ngrok
    vim
    # microcodeAmd
  ];

  # Executables
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
  ];

  # System packages
  hardware.cpu.intel.updateMicrocode = true;
  programs.direnv.enable = true;
  services.fwupd.enable = true;

  # Virtualisation
  virtualisation.docker.enable = true;
  services.flatpak.enable = true;

  # System state version
  system.stateVersion = "24.05"; # Recommended to keep this version
}
